#!/bin/bash 
# CPU
awk '{u=$2+$4; t=$2+$4+$5; if (NR==1){uuu1=u; ttt1=t;} else print ($2+$4-u1) * 100 / (t-t1) ""; }' <(grep 'cpu ' /proc/stat) <(sleep 1;grep 'cpu ' /proc/stat) > /usr/share/jitsi-meet/cpu.txt 

# Memory 
free -m | awk 'NR==2{printf "%s", $3 }' > /usr/share/jitsi-meet/memory.txt 

# Statistic 
rm /usr/share/jitsi-meet/stats.json 
# Change the port when jetty is set to another port.
curl http://127.0.0.1:8080/colibri/stats >> /usr/share/jitsi-meet/stats.json