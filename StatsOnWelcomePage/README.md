# Stats on welcome page

The following description allows the integration of Jitsi Meet stats on the welcome page.

![conference stats](conferencestats.png)

## Enable stats in Videobridge

`nano /etc/jitsi/videobridge/config`

Change the line 
```
JVB_OPTS="--apis=,"
```
to
```
JVB_OPTS="--apis=rest,xmpp"
```

`nano /etc/jitsi/videobridge/sip-communicator.properties`

Check that the following values are set:
```
org.jitsi.videobridge.ENABLE_STATISTICS=true
org.jitsi.videobridge.STATISTICS_TRANSPORT=muc,colibri
```

If the default Jetty port (8080) is already in use (you can find out with `netstat -tulpn`), you have to add this line (e.g. to set the port to 9090):
```
org.jitsi.videobridge.rest.private.jetty.port=9090
```

Then restart the service: `service jitsi-videobridge2 restart`

Check if the stats are working: `curl -v http://127.0.0.1:8080/colibri/stats`

## Add script to collect stats

- `mkdir -p /root/scripts/jitsi/`
- `cd /root/scripts/jitsi/`
-  Add the script jitsi-stats.sh to that directory
- `chmod +x jitsi-stats.sh`

### Add cronjob to collect the stats every 20 seconds

- `crontab -e`
```
* * * * * /root/scripts/jitsi/jitsi-stats.sh
* * * * * sleep 20; /root/scripts/jitsi/jitsi-stats.sh
* * * * * sleep 40; /root/scripts/jitsi/jitsi-stats.sh
```

### Make the stats available with HTTPS (nginx)

- Modify your jitsi-vHost: `nano /etc/nginx/conf.d/meet.meinedomain.de.conf`
- Add the following location block (change the port if Jetty is running on another port):
```
location = /jitsi-stats {
        proxy_pass http://127.0.0.1:8080/colibri/stats;
    }
```
- `service nginx restart`
- Test: https://meet.meinedomain.de/jitsi-stats should provide the stats in JSON format.

### Modify welcome page

- `cd /usr/share/jitsi-meet/static`
- Sample welcome page: welcomePageAdditionalContent.html
- Here you have to set the RAM size in MB.